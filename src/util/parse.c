/**\file parse.c
 *
 * \brief Various methods used for parsing poles and zeros and coefficients files.
 */
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "util.h"
#include "errors.h"
#include "parse.h"

int getLine(FILE *fp, char *buffer, int buflen, char comment, int *lineno) {
	int i;
	clearerr(fp);
	buffer[0] = 0;
	do {
		/*  Read the next line in the file  */
		if (fgets(buffer, buflen - 1, fp) == NULL) {
			buffer[0] = 0;
			if (feof(fp)) {
				return 1;
			} else if (ferror(fp)) {
				return 2;
			} else {
				return 3;
			}
		}
		++*lineno;

		/*  Truncate everything after comment token  */
		if (comment != 0) {
			i = 0;
			while (i < strlen(buffer) && buffer[i++] != comment)
				;
			buffer[--i] = 0;
		}

		/*  Remove trailing blanks  */
		i = strlen(buffer) - 1;
		while (i >= 0 && (buffer[i] == ' ' || buffer[i] == '\n')) {
			--i;
		}
		buffer[++i] = 0;
	} while (strlen(buffer) <= 0);

	return 0;
}


int sparse(char *str, char *result[], char *delimiters, int max_tokens) {
	int i = 0;

	if (max_tokens < 1) {
		errors("sparse: illegal 'max_tokens' (%i)\n",max_tokens);
	}

	if ((result[0] = strtok(str, delimiters)) == NULL) {
		return 0;
	}
	debug(DEBUG_SENSOR_LIST_PARSER, "Read token <%s>\n" , result[0]);
	for (i = 1; i < max_tokens; i++) {
		if ((result[i] = strtok(NULL, delimiters)) == NULL) {
		  debug(DEBUG_SENSOR_LIST_PARSER, "Read token <%s>\n" , result[i]);
		  return i;
		}
    result[i] = strdup(result[i]);
	  debug(DEBUG_SENSOR_LIST_PARSER, "Read token <%s>\n" , result[i]);
	}
	return i;
}


int trim(char *str) {
	int n;

	n = strlen(str);
	if (str[n - 1] == '\n') {
		str[--n] = 0;
	}
	if (str[n - 1] == 0x0d) {
		str[--n] = 0;
	}
	--n;
	while (n >= 0 && str[n] == ' ') {
		--n;
	}
	n++;
	str[n] = '\0';
	return n;
}


int trim_cr(char *str) {
	int n;

	n = strlen(str);
	if (str[n - 1] == '\n') {
		str[--n] = 0;
	}
	if (str[n - 1] == 0x0d) {
		str[--n] = 0;
	}
	str[n] = '\0';
	return n;
}


char *to_upper(char *str) {
	int i;
	char *copy = (char *) mem(strlen(str) + 1);
	for (i = 0; i < strlen(str); i++) {
		copy[i] = toupper(str[i]);
	}
	copy[strlen(str)] = '\0';
	return copy;
}

void ucase(char *str) {
	int i;
	for (i = 0; i < strlen(str); i++) {
		str[i] = toupper(str[i]);
	}
	return;
}


void lcase(char *str) {
	int i;

	for (i = 0; i < strlen(str); i++) {
		str[i] = tolower(str[i]);
	}
	return;
}
