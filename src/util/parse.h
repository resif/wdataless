/*\file parse.h
 *
 */

#ifndef PARSE_H_
#define PARSE_H_

#include <stdio.h>

/**\fn int getLine(FILE *fp, char *buffer, int buflen, char comment, int *lineno)
 *
 * \brief Read a single line from the given file, stripping out comments and blank lines.
 *
 * The processed line will be a NULL terminated string and without
 * the trailing newline.
 *
 * \param fp       input stream
 * \param buffer   buffer to hold line
 * \param buflen   length of buffer
 * \param comment  comment character
 * \param lineno   line number of line in buffer
 *
 * \return The value returned are :
 *            - 0 => success
 *            - 1 => EOF
 *            - 2 => read error
 *            - 3 => other error
 */
int getLine(FILE *fp, char *buffer, int buflen, char comment, int *lineno);

/**\fn int sparse(char *str, char *result[], char *delimiters, int max_tokens)
 *
 * \brief tokenize a string and store each token.
 *
 * \param str        The string to tokenize
 * \param result     The array used to store token
 * \param delimiters All valid delimiters
 * \param max_tokens The size of result array
 *
 * \return The number of tokens
 */
int sparse(char *str, char *result[], char *delimiters, int max_tokens);


/**\fn int trim(char *str)
 *
 * \brief Remove trailing carriage return and trailing blanks.
 *
 * \param str The given string
 *
 * \return The new size of the string
 */
int trim(char *str);


/**\fn int trim_cr(char *str)
 *
 * \brief Remove trailing carriage return but not trailing blanks.
 *
 * \param str The given string
 *
 * \return The new size of the string
 */
int trim_cr(char *str);

/**\fn void to_upper(char *str)
 *
 * \brief Give the upper case representation of a given string
 *
 * \param str The string to convert
 *
 * \return    The upper cased version of given string
 */
char *to_upper(char *str);

/**\fn void ucase(char *str)
 *
 * \brief Convert a given string to upper case.
 *
 * Note: The conversion is made in place
 *
 * \param str The string to convert
 */
void ucase(char *str);


/**\fn inline void lcase(char *str)
 *
 * \brief Convert a given string to lower case.
 *
 * \param str The string to convert
 */
void lcase(char *str);

#endif /* PARSE_H_ */
