/*====================================================================
 libutil.c
 *===================================================================*/
#define _XOPEN_SOURCE 700
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "errors.h"
#include "util.h"

const int upper_char = 1;
const int lower_char = 2;
const int digit_char = 4;
const int punct_char = 8;
const int space_char = 16;
const int under_char = 32;



char *strcut_utf8(char *str, int maxsize) {
  char *result;
  /* If the string size in bytes is less than maxsize */
  if (strlen(str) < maxsize) {
    result = mem(strlen(str) + 1);
    strcpy(result, str);
    return result;
  }

  result = mem(maxsize);
  int i = 0;
  while(i+1<maxsize) {
    int inc;
    /* Get next UTF-8 character */
    switch (0xF0 & str[i]) {
      case 0xD0:
      case 0xC0:
        inc = 2;
        break;
      case 0xE0:
        inc = 3;
        break;
      case 0xF0:
        inc= 4;
        break;
      default:
        inc = 1;
        break;
    }
    /* Copy UTF-8 character if enough place */
    if (i + inc + 1 < maxsize) {
      int j;
      for (j=0; j<inc; j++) {
        result[i+j] = str[i+j];
      }
    }
    i += inc;
  }
  result[maxsize-1] = 0;
  return result;
}

int strlen_utf8(char *s) {
   int i = 0, j = 0;
   while (s[i]) {
     if ((s[i] & 0xc0) != 0x80) j++;
     i++;
   }
   return j;
}


void *mem(size_t size) {
	char *result;

	if ((result =  calloc(size, 1L)) == NULL) {
		errors("calloc failed for %d bytes; ", size);
	}
	return result;
}


void *mem_realloc(void *old,size_t new_size) {
	void *result;
	if((result = realloc(old,new_size)) == NULL) {
		errors("realloc failed for %d bytes\n", new_size);
	}
	return result;
}

int find_wordorder(char *str) {
#define MSB_FIRST 0x76543210         /* MC680x0 word order */
#define MSB_LAST  0x10325476         /* VAX, 80x86 word order */
	union {
		unsigned char character[4];
		unsigned long int integer;
	} test4; /* holds test 4-byte word */

	/* Construct a 4-byte word of known contents - 0x76543210 */
	test4.character[0] = 0x76;
	test4.character[1] = 0x54;
	test4.character[2] = 0x32;
	test4.character[3] = 0x10;

	/* determine the 4-byte word order of this machine */
	if (test4.integer == MSB_FIRST)
		sprintf(str, "321010");
	else if (test4.integer == MSB_LAST)
		sprintf(str, "012301");
	else {
		errors("find_wordorder:  machine word order, %ld, not treated by byte swappers.\n", test4.integer);
	}
	if (test4.integer == MSB_FIRST)
		return 1;
	else
		return 0;
}




/**\fn inline void normalize(char *str,int normalization_flag)
 *
 * \brief Verify string normalization.
 *
 *
 * \param str                The string to verify and correct
 * \param normalization_flag A bitwise flag to define normalized char
 *
 */
inline void normalize(char *str, int normalization_flag) {
	while (*str != '\0') {
		if (!(normalization_flag & upper_char) && isupper((unsigned char)*str)) {
			warnings("Warning the string %s use an upper case char(%c)\n", str,*str);
		}
		if (!(normalization_flag & lower_char) && islower((unsigned char)*str)) {
			warnings("Warning the string %s use a lower case char(%c)\n", str,*str);
		}
		if (!(normalization_flag & digit_char) && isdigit((unsigned char)*str)) {
			warnings("Warning the string %s use a digit char(%c)\n",str, *str);
		}
		if (!(normalization_flag & punct_char) && ispunct((unsigned char)*str)) {
			warnings("Warning the string %s use a punctuation char(%c)\n", str,*str);
		}
		if (!(normalization_flag & space_char) && *str == ' ') {
			warnings("Warning the string %s use a space char(%c)\n",str, *str);
		}
		if (!(normalization_flag & under_char) && *str == '_') {
			warnings("Warning the string %s use an underline char(%c)\n", str,*str);
		}
		str++;
	}
}

int isdir(char *path) {
	struct stat statbuf;

	if (stat(path, &statbuf) != 0) {
		return FALSE;
	}
	if ((statbuf.st_mode & S_IFMT) == S_IFDIR) {
		return TRUE;
	}
	return FALSE;
}


int file_exist_in_pz(char *path) {

	struct stat statbuf;
	char *tmp;

	tmp = mem(strlen(path) + strlen(get_pz_dir()) + 2);
	sprintf(tmp,"%s/%s", get_pz_dir(), path);

	if (stat(tmp, &statbuf) != 0) {
		return FALSE;
	}
	if (S_ISREG(statbuf.st_mode & S_IFMT)) {
		return TRUE;
	}
	return FALSE;
}



char *get_pz_dir() {
	static char *PZdir = NULL;

	if(PZdir != NULL)
		return PZdir;

	if (!(PZdir = getenv("PZ"))) {
		errors("%s\n","The variable 'PZ' \n\tmust be set to the\ndirectory containing responses informations.\n\tExample: setenv PZ /home/fels/respdb/PZ");
	}
	PZdir = strdup(PZdir);
	if (!isdir(PZdir)) {
		errors("directory '%s' not found\n", PZdir);
	}
	return PZdir;
}

/**\var seed_organization
 *
 * \brief The organization writing the seed dataless.
 */
static char *seed_organization = NULL;


void set_seed_organization(char *organization) {
	seed_organization = mem(strlen(organization) + 1);
	strcpy(seed_organization,organization);
}

char *get_seed_organization() {
	if(seed_organization == NULL)
		errors("%s\n","Internal error seed_organization is NULL");
	return seed_organization;
}



