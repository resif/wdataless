#ifndef __LIBSEED_H__
#define __LIBSEED_H__

#include <stdio.h>

#define SEED_REV     2.4
#define PATHLEN 255
//#define SUN   1
#define LINUX 0

//typedef struct {
//  int year;
//  int month;
//  int day;
//  int hour;
//  int min;
//  float sec;
//} Tstruct;

#define leap_year(i) ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)



#define LREC_LEN         "12"      /* Logical record length: 1**12=4096 */
//#define FSDH_SIZE         48       /* Fixed Section Data Header size */
//#define DATA_REC_HD_SIZE 128       /* Data Header size */
//#define BYTES_IN_DREC  (LRECL - DATA_REC_HD_SIZE)

typedef enum data_format {
  STEIM1 = 1, STEIM2, IEEEFLOAT, INT32
} data_format_t;

/* Times outside 1990.01.01-00:00:00 - 2036.07.18-13:20:00 are bad */
#define END_OF_WORLD   (double) 2100000000
#ifndef TIME_OK
#define TIME_OK(t) (((int) t>631152000 && (int) t<(int) END_OF_WORLD) ? 1:0)
#endif



/*  Constants  */

#ifndef PI
#define PI (double) 3.14159265358979
#endif
#define TWOPI (double) 2.0 * PI
#define FIR_NORM_TOL   0.02
//#define MAXTOKENS     30
//#define NCOMP          3


#ifndef UNKNOWN
#define UNKNOWN        0x1777
#endif

#include "station.h"
#include "filter.h"

/*  Structure templates  */

#define STALOCID  "--"

/**\fn int calc_channel_sint(station_t station)
 *
 * \brief Compute channel sample interval.
 *
 * \param station The station
 *
 * \return TRUE if no errors and stop program otherwise.
 */
int calc_channel_sint(station_t sta);


void check_sensor_orient(station_t sta);


/**\fn int check_sym(filter_t filter)
 *
 * \brief Verify coefficients normalization and symmetry.
 *
 * \param filter The filter to verify.
 *
 * \return FALSE in case of errors and TRUE otherwise.
 */
int check_sym(filter_t filter);


/**\fn int check_roots(double root[], int nroot, int type, char *p_z)
 *
 * \brief Check real an imag parts of roots.
 *
 * \param root
 * \param nroot The number of roots in root array
 * \param type
 * \param p_z
 */
int check_roots(double root[], int nroot, int type, char *p_z);

int dbencode(char *string);

#endif
