/**\file write_seed.h
 *
 */

#ifndef WRITE_SEED_H_
#define WRITE_SEED_H_

#define LRECL      4096
#define PRECL     (LRECL * 8)

/**\fn void free_VH()
 *
 * \brief Clear volume header list
 */
void free_VH(void);

/**\fn void free_AH()
 *
 * \brief Clear abbreviation header list
 */
void free_AH(void);

/**\fn void free_SH()
 *
 * \brief Clear station header list
 */
void free_SH(void);

/**\fn int find_nVH()
 *
 * \brief Compite the number of volume header according to the number of station.
 *
 * \return The number of volume header.
 */
int find_nVH();

/**\fn void save_volume_logical_volume(char *data)
 *
 * \brief Save
 */
void save_volume_logical_volume(char *data);

/**\fn void save_abbreviation_logical_volume(char *data)
 *
 */
void save_abbreviation_logical_volume(char *data);

/**\fn void save_station_logical_volume(char *data)
 *
 */
void save_station_logical_volume(char *data);

/**\fn char *new_volume_logical_volume(char *data,int save)
 *
 * \brief
 */
void new_volume_logical_volume(char *data, int save);

/**\fn char *new_abbreviation_logical_volume(char *data,int save);
 *
 */
void new_abbreviation_logical_volume(char *data, int save);

/**\fn char *new_station_logical_volume(char *data,int save);
 *
 */
void new_station_logical_volume(char *data, int save);

/**\fn void write_control_logical_volumes(char *vol_name,FILE *seed_file,int calculated_nVH);
 *
 * \brief Write volumes logical volumes in given file.
 *
 * \param vol_name       The name of the seed dataless file
 * \param seed_file      The FILE * structure
 * \param calculated_nVH The pre-calculated number of volume index control headers
 *
 */
void write_control_logical_volumes(char *vol_name, FILE *seed_file,
    int calculated_nVH);

/**\fn void write_abbreviation_logical_volumes(char *vol_name,FILE *seed_file)
 *
 * \brief Write abbreviation logical volumes in given file.
 *
 * \param vol_name  The name of the seed dataless file
 * \param seed_file      The FILE * structure
 */
void write_abbreviation_logical_volumes(char *vol_name, FILE *seed_file);

/**\fn void write_station_logical_volumes(char *vol_name, FILE *seed_file)
 *
 * \brief Write station logical volumes in given file.
 *
 * \param vol_name  The name of the seed dataless file
 * \param seed_file The FILE * structure
 */
void write_station_logical_volumes(char *vol_name, FILE *seed_file);

/**\fn void close_dataless_file(char *vol_name, FILE *seed_file)
 *
 * \brief Finish to write dataless seed (padding with space) and close file.
 *
 * \param vol_name  The name of the seed dataless file
 * \param seed_file The FILE used to write dataless
 */
void close_dataless_file(char *vol_name, FILE *seed_file);

#endif /* WRITE_SEED_H_ */
