
#include <string.h>
#include "comment.h"
#include "util.h"
#include "time_date.h"



int comp_comment(comment_t c1, comment_t c2) {
  return strcmp(c1->label, c2->label);
}

int have_label(comment_t comment, char *value) {
  return strcmp(comment->label, value);
}
comment_t create_comment(char *label, char *value, char *begin_time, char *end_time, list_t author, int lookup_key, comment_type_t type) {

  comment_t result;

  result = (comment_t) mem(sizeof(struct comment_s));
  result->label = label;
  result->value = value;

  // Process begin date
  if (!strcmp(end_time, "none")) {
    result->begin_time = NULL;
  } else {
    result->begin_time = (double *)mem(sizeof(double));
    *(result->begin_time) = asc2dbtime(begin_time);
  }

  // Process end date
  if (!strcmp(end_time, "present") || !strcmp(end_time, "none")) {
    result->end_time= NULL;
  } else {
    result->end_time = (double *)mem(sizeof(double));
    *(result->end_time) = asc2dbtime(end_time);
  }

  result->author = author;
  result->seed_key = lookup_key;
  result->type = type;

  return result;
}
