/**\file units.c
 *
 */

#include <string.h>
#include <stdlib.h>
#include "main.h"
#include "units.h"
#include "errors.h"
#include "util.h"
#include "parse.h"

static list_t unitListHead = NULL;

void read_unit_list() {
  static int unit_list_loaded = 0;
  int next_lookup_code = 1;
  int lineno = 0;
  FILE *Fp = NULL;
  unit_t pt;
  char fname[511];
  char line[LINE_MAX_SIZE];
  char line_cp[LINE_MAX_SIZE];
  char *token[50];
  int ntokens;
  char *PZdir;

  if (!unit_list_loaded) {
    unit_list_loaded = 1;
    if ((PZdir = getenv("PZ"))) {
      if (!isdir(PZdir)) {
        errors("'%s' don't exists or not a directory\n", PZdir);
      }
    } else {
      errors("%s\n",
          "\tERROR: read_unit_list:\n\tThe 'unit_list' file is mandatory .\n\tThis file is located in the directory pointed to by the environment\n\tvariable 'PZ'.");
      exit(1);
    }
    sprintf(fname, "%s/%s", PZdir, UNITLIST);
    if (!(Fp = fopen(fname, "r"))) {
      errors("can't open %s\n", fname);
    }

    while (!getLine(Fp, line, LINE_MAX_SIZE, '#', &lineno)) {
      int i;

      trim(line);
      sprintf(line_cp, "%s", line);
      ntokens = sparse(line, token, " \t", 50);
      if (ntokens < 2) {
        errors("file '%s': need at least 2 tokens in line '%s'\n", fname,
            line_cp);
      }
      pt = (unit_t) calloc(sizeof(struct unit_s), 1L);
      unitListHead = append_element(pt, unitListHead);
      if (strlen(token[0]) + 1 > UNIT_NAME_SIZE) {
        errors("In <%s>  name length is greater than maximum (%i)\n",
            line_cp, UNIT_NAME_SIZE-1);
      }
      sprintf(pt->name, "%s", token[0]);
      if (strlen(token[1]) + 1 > UNIT_NAME_SIZE) {
        errors("In <%s>  seed code length is greater than maximum (%i)\n",
            line_cp, UNIT_NAME_SIZE-1);
      }
      sprintf(pt->seed_code, "%s", token[1]);

      int desc_size = 0;
      for (i = 2; i < ntokens; i++) {
        strncpy(pt->description + desc_size, token[i],
            strlen(token[i]) >= (UNIT_DESC_SIZE - desc_size - 1) ? (UNIT_DESC_SIZE - desc_size)
                - 1 :
                strlen(token[i]));
        if (desc_size + strlen(token[i]) >= UNIT_DESC_SIZE - 1)
          break;
        *(pt->description + desc_size + strlen(token[i])) = ' ';
        desc_size += strlen(token[i]) + 1;
      }
      pt->lookup_code = next_lookup_code++;
      debug(DEBUG_UNIT_LIST_PARSER, "Read :<%s> <%s> <%s>\n", pt->name,
            pt->seed_code, pt->description);
    }

    fclose(Fp);
    Fp = NULL;
    return;
  }
}

list_t get_unit_list_head() {
  return unitListHead;
}

unit_t get_unit_from_string(char *string_unit) {
  list_t  current_unit_node = unitListHead;
  char   *upper_string_unit = to_upper(string_unit);

  while(current_unit_node) {
    unit_t current_unit = (unit_t) current_unit_node->element;
    if (strcmp(current_unit->name, upper_string_unit) == 0) {
      return current_unit;
    }
    current_unit_node = current_unit_node->next;
  }
  errors("unsupported units %s\n",
      string_unit);
  return NULL;
}
