/**\file analog_filter.h
 *
 * \brief Analog filter management header file.
 *
 */

#ifndef ANALOG_FILTER_H_
#define ANALOG_FILTER_H_

#include "filter.h"
#include "units.h"


/**\struct analog_filter_stage_s
 *
 * \brief Analog filter stage data representation.
 */
struct analog_filter_stage_s {
  double    ampli_G;     /**< Filter gain                    */
  filter_t  filter;      /**< analog filter                  */
  unit_t    inpUnits;    /**< units: ACC | AMP               */
  unit_t    outUnits;    /**< units: ACC | AMP               */
};

/**
 * Analog filter stage type.
 */
typedef struct analog_filter_stage_s *analog_filter_stage_t;

/**\struct analog_filter_s
 *
 * \brief Analog filter data representation.
 */
struct analog_filter_s {
  long                   object_type; /**< The object type                */
  char                   label[10];   /**< Analog filter label            */
  char                  *fname;       /**< analog filter filter file name */
  int                    nstage;      /**< Number of analog filter stage  */
  analog_filter_stage_t *stages;      /**< analog filter                  */
};

/**
 * Analog filter type.
 */
typedef struct analog_filter_s *analog_filter_t;

/**\fn int comp_analog_filter(analog_filter_t filter1, analog_filter_t filter2);
 *
 * \brief say if two analog_filter_t have the same label.
 *
 * filter1 -- The first analog filter
 * filter2 -- The second analog filter
 *
 * return  -- 0 if the analog filters have the same label.
 */
int comp_analog_filter(analog_filter_t filter1, analog_filter_t filter2);

/**\fn int is_analog_filter(analog_filter_t analog_filter)
 *
 * \brief Say if a given pointer is an analog_filter_t.
 *
 * \param analog_filter The tested pointer
 *
 * \return TRUE if the given pointer is an analog_filter_t and FALSE otherwise.
 */
int is_analog_filter(analog_filter_t analog_filter);

/**\fn analog_filter_t create_analog_filter(char *label,char *stagefile)
 *
 * \brief Create a new analog filter.
 *
 * \param label     The label of this analog filter
 * \param stagefile The stagefile used to create analog filter
 *
 * \return The newly created analog filter
 */
analog_filter_t create_analog_filter(char *label, char *stagefile);

/**\fn analog_filter_stage_t create_analog_filter_stage()
 *
 * \brief Create a new analog filter stage.
 *
 * \return The newly created analog filter stage
 */
analog_filter_stage_t create_analog_filter_stage();

/**\fn void process_analog_filter(analog_filter_stage_t analog_filter_stage,definition_t definition_list)
 *
 * \brief Update a given analog filter stage according to value present in the definitions list.
 *
 * \param analog_filter       The analog filter hosting analog filter stage to process
 * \param analog_filter_stage The analog filter stage to update
 * \param definition_list     The definitions list
 *
 */
void process_analog_filter_stage(analog_filter_t analog_filter, analog_filter_stage_t analog_filter_stage,
    definition_t definition_list);

#endif /* ANALOG_FILTER_H_ */
