
#ifndef PHONE_NUMBER_H_
#define PHONE_NUMBER_H_

typedef struct phone_number_s *phone_number_t;

struct phone_number_s {
  int  *country_code;
  int   area_code;
  char *phone_number;
};

phone_number_t create_phone_number(int *country_code, int area_code, char *phone_number);

#endif
