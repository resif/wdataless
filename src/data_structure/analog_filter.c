/**\file analogic_filter.c
 *
 * \brief Analogic filter structures filling and manipulation.
 */
#include <stdlib.h>
#include <string.h>
#include "seed.h"
#include "seed.h"
#include "definition.h"
#include "coef_file.h"
#include "errors.h"
#include "analog_filter.h"
#include "filter.h"
#include "util.h"
#include "units.h"
#include "main.h"
#include "stage.h"

static long ANALOG_FILTER_TYPE = 0xA2A1061C;

int comp_analog_filter(analog_filter_t filter1, analog_filter_t filter2) {
  return strcmp(filter1->label, filter2->label);
}

int is_analog_filter(analog_filter_t analogic_filter) {
  return analogic_filter != NULL && analogic_filter->object_type == ANALOG_FILTER_TYPE;
}

analog_filter_t create_analog_filter(char *label, char *stagefile) {

  analog_filter_t result = (analog_filter_t) mem(sizeof(struct analog_filter_s));
  char ampliFname[511];

  result->object_type = ANALOG_FILTER_TYPE;
  sprintf(result->label, "%s", label);

  sprintf(ampliFname, "%s/%s", get_pz_dir(), stagefile);
  result->fname = (char *) mem(strlen(ampliFname) + 3);
  sprintf(result->fname, "%s", ampliFname);

  if (stagefile != NULL) {
      result->fname = (char *) mem(strlen(get_pz_dir()) + strlen(stagefile) + 2);
      sprintf(result->fname, "%s/%s", get_pz_dir(), stagefile);

      parse_stage_file(result->fname, result);
  }
  return result;
}

analog_filter_stage_t create_analog_filter_stage() {
  analog_filter_stage_t result = (analog_filter_stage_t) mem(sizeof(struct analog_filter_stage_s));
  result->ampli_G = 1.0;
  result->filter = NULL;

  return result;
}

void process_analog_filter_stage(analog_filter_t analog_filter, analog_filter_stage_t analog_filter_stage, definition_t definition_list) {
  definition_t current = definition_list;


  int findTransferFunction          = FALSE;
  int findSensitivity               = FALSE;
  int findTransferNormalisationFreq = FALSE;

  while (current != NULL) {
    if (!strcasecmp(current->value_name, "TRANSFER_FUNCTION_TYPE")) {
      if (analog_filter_stage->filter == NULL) {
        analog_filter_stage->filter = create_filter();
      }
      if (!strcasecmp((char *) current->value, "ANALOG")) {
        analog_filter_stage->filter->type = ANALOG;
      } else if (!strcasecmp((char *) current->value, "LAPLACE")) {
        analog_filter_stage->filter->type = LAPLACE;
      } else {
        errors("Transfer function type is '%s' and must be 'ANALOG' or 'LAPLACE' in %s\n",
                    (char *) current->value, analog_filter->fname);
      }
      findTransferFunction =TRUE;
    } else if (!strcasecmp(current->value_name, "INPUT_UNIT")) {
      analog_filter_stage->inpUnits = get_unit_from_string((char *) current->value);
      if (strcmp(analog_filter_stage->inpUnits->name, "V")) {
        errors("Input unit of analogical filter must be Volt in %s\n", analog_filter->fname);
      }
    } else if (!strcasecmp(current->value_name, "OUTPUT_UNIT")) {
      analog_filter_stage->outUnits = get_unit_from_string((char *) current->value);
      if (strcmp(analog_filter_stage->outUnits->name, "V")) {
        errors("Output unit of analogical filter must be Volt in %s\n", analog_filter->fname);
      }
    } else if (!strcasecmp(current->value_name, "SENSITIVITY")) {
      analog_filter_stage->ampli_G = *((double *) current->value);
      analog_filter_stage->filter->Sd = *((double *) current->value);
      findSensitivity = TRUE;
    } else if (!strcasecmp(current->value_name,
        "TRANSFER_NORMALIZATION_FREQUENCY")) {
      if (analog_filter_stage->filter == NULL) {
        analog_filter_stage->filter = create_filter();
      }
      analog_filter_stage->filter->fn = *((double *) current->value);
      if (analog_filter_stage->filter->fn  < 0) {
         errors("TRANSFER_NORMALIZATION_FREQUENCY must be greater than 0 (%e) in %s\n",
                analog_filter_stage->filter->fn, analog_filter->fname);
       }
     findTransferNormalisationFreq = TRUE;
    } else if (!strcasecmp((char *) current->value_name, "INCLUDE")) {
      // Must load pz file
      if (analog_filter_stage->filter == NULL) {
        analog_filter_stage->filter = create_filter();
      }
      if (read_poles_and_zeros((char *) current->value, (filter_t) analog_filter_stage->filter) == FALSE) {
        errors("readPZ failed for %s\n", analog_filter->fname);
      }
    } else if (!strcasecmp((char *) current->value_name, "NUMBER_OF_ZEROES")) {
      /* Nothing to do */
    } else if (!strcasecmp((char *) current->value_name, "NUMBER_OF_POLES")) {
      /* Nothing to do */
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_COEFFICIENTS")) {
      errors("%s\n", "NUMBER_OF_COEFFICIENTS is not a valid keyword for analog filter");
    } else if (!strcasecmp(current->value_name, "RESPONSE_NAME")) {
      /* Nothing to do */
    } else if (!strcasecmp(current->value_name, "RESPONSE_TYPE")) {
      /* Nothing to do */
    } else {
      warnings("The keyword '%s' is unknown so we skip it\n",current->value_name);
    }
    current = current->next;
  }

  // Verify that mandatory definitions is presents
  if(!findTransferFunction) {
    errors("Can't find TRANSFER_FUNCTION_TYPE in %s\n", analog_filter->fname);
  }


  if(!findSensitivity) {
    errors("Can't find SENSITIVITY in %s\n", analog_filter->fname);
  }

  if(findTransferNormalisationFreq && analog_filter_stage->filter->nzero == 0 && analog_filter_stage->filter->npole == 0) {
    errors("TRANSFER_NORMALIZATION_FREQUENCY is not valid for analog filter without poles and zeroes (%s)\n", analog_filter->fname);
  }

  /**
   *  No analog filter associated to amplifier
   */
  if (analog_filter_stage->filter->nzero == 0 && analog_filter_stage->filter->npole == 0) {
    free(analog_filter_stage->filter);
    analog_filter_stage->filter = NULL;
    return;
  }

  if (check_roots(analog_filter_stage->filter->pole, analog_filter_stage->filter->npole,
      analog_filter_stage->filter->type, "pole") == FALSE) {
    errors("check roots poles failed in %s\n", analog_filter->fname);
  }

  if (check_roots(analog_filter_stage->filter->zero, analog_filter_stage->filter->nzero,
      analog_filter_stage->filter->type, "zero") == FALSE) {
        errors("check roots zeroes failed in %s\n", analog_filter->fname);
  }
  calc_filter_A0(analog_filter_stage->filter);
//  if (verify_normalization_frequency(analog_filter_stage->filter) != 0) {
//    errors("Error when checking normalization frequency in %s\n", analog_filter->fname);
//  }
  return;
}
