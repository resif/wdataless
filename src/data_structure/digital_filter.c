/**\file digital_filter.c
 *
 * \brief Digital filter manipulation methods..
 *
 */
#include <string.h>
#include "main.h"
#include "stage.h"
#include "util.h"
#include "definition.h"
#include "errors.h"
#include "coef_file.h"
#include "digital_filter.h"

/**\var CASCADE_TYPE
 *
 * \brief Unique id code for filter_cascade_t objects.
 */
static long CASCADE_TYPE = 0xCA2CADE;

/**\var DIG_FILTER_TYPE
 *
 * \brief Unique id code for digital_filter_t objects.
 */
static long DIG_FILTER_TYPE = 0xD16F11;

int comp_cascade(filter_cascade_t cascade1, filter_cascade_t cascade2) {
  return strcmp(cascade1->label, cascade2->label);
}


int is_cascade(filter_cascade_t cascade) {
  return cascade != NULL && cascade->object_type == CASCADE_TYPE;
}

int is_digital_filter(digital_filter_t filter) {
  return filter != NULL && filter->object_type == DIG_FILTER_TYPE;
}

filter_cascade_t create_digital_filter_cascade(char *label, char *stagefile) {
  filter_cascade_t result = (filter_cascade_t) mem(sizeof(struct filter_cascade_s));

  debug(DEBUG_DATA_STRUCTURE,"Begin creation of %s (stagefile=%s)\n",label,stagefile);

  result->object_type = CASCADE_TYPE;
  sprintf(result->label, "%s", label);
  if (stagefile != NULL) {
    result->filename = (char *) mem(strlen(get_pz_dir()) + strlen(stagefile) + 2);
    sprintf(result->filename, "%s/%s", get_pz_dir(), stagefile);
    result->is_fake = 0;
    parse_stage_file(result->filename, result);
  }
  debug(DEBUG_DATA_STRUCTURE,"%s is created\n",label);
  return result;
}

digital_filter_t create_digital_filter() {
  digital_filter_t result = (digital_filter_t) mem(
      sizeof(struct digital_filter_s));
  result->object_type = DIG_FILTER_TYPE;
  result->filter = (filter_t) mem(sizeof(struct filter_s));
  return result;
}

void process_digital_filter_stage(digital_filter_t stage,
    definition_t definitions_list) {

  definition_t   current                       = definitions_list;
  filter_type_t  type                          = (filter_type_t) 0;
  int            decim                         = 0;
  double         normalization_frequency       = 0;
  char          *coef_fname                    = NULL;
  int            findTransferFunction          = FALSE;
  int            findInputSamplingInterval     = FALSE;
  int            findDecimationFactor          = FALSE;
  int            findTransferNormalisationFreq = FALSE;


  while (current != NULL) {
    if (!strcasecmp(current->value_name, "TRANSFER_FUNCTION_TYPE")) {
      if (!strcasecmp((char *) current->value, "fir_sym_2")) {
        type = FIR_SYM_2;
      } else if (!strcasecmp((char *) current->value, "fir_sym_1")) {
        type = FIR_SYM_1;
      } else if (!strcasecmp((char *) current->value, "fir_asym")) {
        type = FIR_ASYM;
      } else if (!strcasecmp((char *) current->value, "iir_poly")) {
      	type = IIR_POLY;
      } else if (!strcasecmp((char *) current->value, "iir_pz")) {
        type = IIR_PZ;
      } else {
        errors("unsupported filter type %s\n",(char *)current->value);
      }
      stage->filter->type = type;
      findTransferFunction = TRUE;
    } else if (!strcasecmp(current->value_name, "INPUT_SAMPLING_INTERVAL")) {
      stage->filter->sint = *((double *) current->value);
      findInputSamplingInterval = TRUE;
    } else if (!strcasecmp(current->value_name, "DECIMATION_FACTOR")) {
      decim = stage->decim = *((double *) current->value);
      stage->filter->decim = *((double *) current->value);
      findDecimationFactor = TRUE;
    } else if (!strcasecmp(current->value_name,
        "TRANSFER_NORMALIZATION_FREQUENCY")) {
      normalization_frequency = *((double *) current->value);
      if (normalization_frequency  < 0) {
         errors("TRANSFER_NORMALIZATION_FREQUENCY must be greater than 0 (%e) in %s\n",
                normalization_frequency, stage->filename);
       }
     findTransferNormalisationFreq = TRUE;
    } else if (!strcasecmp(current->value_name, "INCLUDE")) {
      coef_fname = (char *)mem(strlen((char *) current->value) + 1);
      sprintf(coef_fname, "%s", (char *) current->value);
      stage->filename = (char *) mem(strlen(coef_fname) + 3);
      sprintf(stage->filename, "%s", coef_fname);
    } else if (!strcasecmp(current->value_name, "DELAY")) {
      double delay = *((double *) current->value);
      stage->filter->delay = delay;
    } else if (!strcasecmp(current->value_name, "CORRECTION")) {
      double correction = *((double *) current->value);
      stage->filter->correction = correction;
    } else if (!strcasecmp(current->value_name, "RESPONSE_NAME")) {
      /* Nothing to do */
    } else if (!strcasecmp(current->value_name, "RESPONSE_TYPE")) {
      /* Nothing to do */
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_POLES")) {
      errors("%s\n", "NUMBER_OF_POLES is not a valid keyword for digital filter stage");
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_ZEROES")) {
      errors("%s\n", "NUMBER_OF_ZEROES is not a valid keyword for digital filter stage");
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_COEFFICIENTS")) {
      /* Nothing to do */
    } else {
      warnings("The keyword %s is unknown so we skip it\n",current->value_name);
    }
    current = current->next;
  }

  if (findTransferFunction == FALSE) {
    errors("can't find TRANSFER_FUNCTION_TYPE for %s\n", stage->filename);
  }

  if (findInputSamplingInterval == FALSE) {
    errors("can't find INPUT_SAMPLING_INTERVAL int %s\n", stage->filename );
  }

  if (findDecimationFactor == FALSE) {
    errors("can't find DECIMATION_FACTOR for %s\n", stage->filename);
  }

  if (stage->filter->type == IIR_PZ && findTransferNormalisationFreq == FALSE) {
    errors("can't find TRANSFER_NORMALIZATION_FREQUENCY and filter type is IIR_PZ in %s\n",
           stage->filename);
  }

  if (stage->filter->type != IIR_PZ && findTransferNormalisationFreq != FALSE) {
    warnings("find TRANSFER_NORMALIZATION_FREQUENCY and filter type is not IIR_PZ in %s\n",
             stage->filename);
  }


  stage->filter->nzero = stage->filter->npole = 0;
  stage->filter->A = 1.0;
  stage->filter->gain = 1.0;
  stage->filter->fn = normalization_frequency;
  stage->filter->A0 = 1.0;
  stage->filter->decim = decim;
  stage->filter->type = type;

  if (coef_fname != NULL) {
    if (stage->filter->type == IIR_POLY) {
  	    read_iir_coef(coef_fname, (filter_t) stage->filter);
    } else if (stage->filter->type == IIR_PZ) {
      if (read_poles_and_zeros(coef_fname, (filter_t) stage->filter) == FALSE) {
        errors("readPZ failed for %s\n", coef_fname);
      }
      calc_filter_A0((filter_t) stage->filter);
// Commented, wait for more precise explaination
//      if (verify_normalization_frequency(stage->filter) != 0) {
//        errors("Error when checking normalization frequency in %s\n", stage->filename);
//      }
	  } else {
	    firCoef_t coef = NULL;
	    read_fir_coef(coef_fname, &coef, decim, type);
	    stage->filter->ncoef = coef->ncoef;
	    stage->filter->coef = coef->coef;
	  }
  }
  debug(DEBUG_DATA_STRUCTURE, "The stage contains %i coefs\n", stage->filter->ncoef);
}

void process_digital_filter(digital_filter_t digital_filter,
    definition_t definitions_list) {

  definition_t   current                       = definitions_list;
  filter_type_t  type                          = (filter_type_t)0;
  char          *coef_fname                    = NULL;
  double         normalization_frequency       = 0;
  int            findTransferFunction          = FALSE;
  int            findInputSamplingInterval     = FALSE;
  int            findDecimationFactor          = FALSE;
  int            findTransferNormalisationFreq = FALSE;
  digital_filter->is_fake = 0;

  while (current != NULL) {
    if (!strcasecmp(current->value_name, "TRANSFER_FUNCTION_TYPE")) {
      if (!strcasecmp((char *) current->value, "fir_sym_2")) {
        type = FIR_SYM_2;
        digital_filter->filter->type = type;
      } else if (!strcasecmp((char *) current->value, "fir_sym_1")) {
        type = FIR_SYM_1;
        digital_filter->filter->type = type;
      } else if (!strcasecmp((char *) current->value, "fir_asym")) {
        type = FIR_ASYM;
        digital_filter->filter->type = type;
      } else if (!strcasecmp((char *) current->value, "iir_poly")) {
      	type = IIR_POLY;
      	digital_filter->filter->type = type;
      } else {
        errors("unsupported filter type %s\n", (char *) current->value);
      }
      findTransferFunction = TRUE;
    } else if (!strcasecmp(current->value_name, "RESPONSE_TYPE")) {
      if (!strcasecmp((char *) current->value, "fake")) {
        digital_filter->is_fake = 1;
        return;
      }
    } else if (!strcasecmp(current->value_name, "INPUT_SAMPLING_INTERVAL")) {
      digital_filter->filter->sint = *((double *) current->value);
      findInputSamplingInterval = TRUE;
    } else if (!strcasecmp(current->value_name, "DECIMATION_FACTOR")) {
      digital_filter->decim = *((double *) current->value);
      findDecimationFactor = TRUE;
      digital_filter->filter->decim = *((double *) current->value);
    } else if (!strcasecmp(current->value_name,
        "TRANSFER_NORMALIZATION_FREQUENCY")) {
      normalization_frequency = *((double *) current->value);
      if (normalization_frequency  < 0) {
         errors("TRANSFER_NORMALIZATION_FREQUENCY must be greater than 0 (%e) in %s\n",
                normalization_frequency, digital_filter->filename);
      }
      findTransferNormalisationFreq = TRUE;
    } else if (!strcasecmp(current->value_name, "DELAY")) {
      digital_filter->filter->delay = *((double *) current->value);
    } else if (!strcasecmp(current->value_name, "CORRECTION")) {
      digital_filter->filter->correction = *((double *) current->value);
    } else if (!strcasecmp(current->value_name, "RESPONSE_NAME")) {
      /* Nothing to do */
    } else if (!strcasecmp(current->value_name, "INCLUDE")) {
      coef_fname = (char *)mem(strlen((char *) current->value) + 1);
      sprintf(coef_fname, "%s", (char *) current->value);
      digital_filter->filename = (char *) mem(strlen(coef_fname) + 3);
      sprintf(digital_filter->filename, "%s", coef_fname);
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_POLES")) {
      errors("%s\n", "NUMBER_OF_POLES is not a valid keyword for digital filter");
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_ZEROES")) {
      errors("%s\n", "NUMBER_OF_ZEROES is not a valid keyword for digital filter");
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_COEFFICIENTS")) {
      /* Nothing to do */
    } else {
      warnings("The keyword %s is unknown so we skip it\n",current->value_name);
    }
    current = current->next;
  }

  if (findTransferFunction == FALSE) {
    errors("can't find TRANSFER_FUNCTION_TYPE for %s\n", digital_filter->filename);
  }

  if (findInputSamplingInterval == FALSE) {
    errors("can't find INPUT_SAMPLING_INTERVAL int %s\n", digital_filter->filename );
  }

  if (findDecimationFactor == FALSE) {
    errors("can't find DECIMATION_FACTOR for %s\n", digital_filter->filename);
  }

  if (digital_filter->filter->type == IIR_PZ && findTransferNormalisationFreq == FALSE) {
    errors("can't find TRANSFER_NORMALIZATION_FREQUENCY and filter type is IIR_PZ in %s\n",
           digital_filter->filename);
  }

  if (digital_filter->filter->type != IIR_PZ && findTransferNormalisationFreq != FALSE) {
    warnings("find TRANSFER_NORMALIZATION_FREQUENCY and filter type is not IIR_PZ in %s\n",
             digital_filter->filename);
  }



  digital_filter->filter->nzero = digital_filter->filter->npole = 0;
  digital_filter->filter->A = 1.0;
  digital_filter->filter->gain = 1.0;
  digital_filter->filter->fn = normalization_frequency;
  digital_filter->filter->A0 = 1.0;

  if (coef_fname != NULL) {
    if (digital_filter->filter->type == IIR_POLY) {
  	   read_iir_coef(coef_fname, (filter_t) digital_filter->filter);
	  } else if (digital_filter->filter->type == IIR_PZ) {
      if (read_poles_and_zeros(coef_fname, (filter_t) digital_filter->filter) == FALSE) {
        errors("readPZ failed for %s\n", coef_fname);
      }

      calc_filter_A0((filter_t) digital_filter->filter);
      if (verify_normalization_frequency(digital_filter->filter) != 0) {
        errors("Error when checking normalization frequency in %s\n", digital_filter->filename);
      }
	  } else {
	  	firCoef_t coef;
	  	read_fir_coef(coef_fname, &coef, digital_filter->filter->decim, type);

	  	digital_filter->filter->ncoef = coef->ncoef;
	  	digital_filter->filter->coef = coef->coef;
	  }
  }
  debug(DEBUG_DATA_STRUCTURE, "The digital filter contains %i coefs\n", digital_filter->filter->ncoef);
}
