/**\file author.h
 *
 * \brief Comment author management header file.
 *
 */

#ifndef AUTHOR_H_
#define AUTHOR_H_

#include "list.h"

typedef struct author_s *author_t;

struct author_s {
  char     label[10]; /**< Author dbird label */
  list_t   name;      /**< Name list of this author */
  list_t   agency;    /**< Agency name list of this author */
  list_t   email;     /**< Email list of this author */
  list_t   phone;     /**< Phone number list of this author */
  author_t next;      /**< Used to link all author */
};


/**\fn author_t create_author(list_t name, list_t agency, list_t email, list_t phone)
 *
 * \brief Create a new author structure.
 *
 * \param label  The author dbird label
 * \param name   The author name list
 * \param agency The author agency name list
 * \param email  The author email list
 * \param phone  The author phone list
 *
 * \return       The new created author
 */
author_t create_author(char *label, list_t name, list_t agency, list_t email, list_t phone);

/**\fn int author_label_comp(author_t author, char *label_value)
 *
 * \brief Say if a given author have a given label
 *
 * \param author      The author
 * \param label_value The label value
 *
 * \return            0 if author label is equal to label_value
 *                    and a non zero value otherwise.
 */
int author_label_comp(author_t author, char *label_value);

#endif
