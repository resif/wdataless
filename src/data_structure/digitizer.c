/**\file digitizer.c
 *
 *
 */
#include <string.h>
#include "digitizer.h"
#include "stage.h"
#include "definition.h"
#include "errors.h"
#include "util.h"
#include "seed.h"
#include "main.h"

/**\var ADC_TYPE
 *
 * \brief Unique id code for digitizer_t objects.
 */
static long ADC_TYPE = 0xADC1143;

int comp_digitizer(digitizer_t digitizer1, digitizer_t digitizer2) {
  return strcmp(digitizer1->label, digitizer2->label);
}

int is_adc(digitizer_t adc) {
  return  adc != NULL && adc->object_type == ADC_TYPE;
}

digitizer_t create_adc(char *label, char *stage_file) {
	digitizer_t result;
	char digitFname[511];

	debug(DEBUG_DATA_STRUCTURE, "Begin creation of adc %s\n",label);
	result = (digitizer_t) mem(sizeof(struct digitizer_s));

	result->object_type = ADC_TYPE;
	sprintf(result->label, "%s", label);
	result->dataformat = 0;

	sprintf(digitFname, "%s/%s", get_pz_dir(), stage_file);
	/* Parse stage file */
	parse_stage_file(digitFname,result);
	debug(DEBUG_DATA_STRUCTURE,"adc %s is created\n",result->label);
	return result;
}


void process_adc(digitizer_t adc,definition_t definition_list) {
	definition_t current = definition_list;
	int findTransfertFunctionType  = FALSE;
	int findSensitivity            = FALSE;
	int findOutputSamplingInterval = FALSE;

	/* First of all manage definitions*/
	while(current != NULL) {
		if (!strcasecmp(current->value_name, "TRANSFER_FUNCTION_TYPE")) {
			if (!strcasecmp((char *)current->value, "AD_CONVERSION")) {
				findTransfertFunctionType = TRUE;
			}
		} else if (!strcasecmp(current->value_name, "OUTPUT_SAMPLING_INTERVAL")) {
			adc->sint = *((double *)current->value);
			findOutputSamplingInterval = TRUE;
		} else if (!strcasecmp(current->value_name, "SENSITIVITY")) {
			adc->fact = *((double *)current->value);
			findSensitivity = TRUE;
    } else if (!strcasecmp(current->value_name, "INPUT_UNIT")) {
      unit_t in_unit = get_unit_from_string((char *) current->value);
      if (strcmp(in_unit->name,"V")) {
        errors("Input unit of digitizer must be Volt (V) for %s\n", adc->label);
      }
		} else if (!strcasecmp(current->value_name, "OUTPUT_UNIT")) {
      unit_t out_unit = get_unit_from_string((char *) current->value);
      if (strcmp(out_unit->name, "COUNTS")) {
        errors("Output unit of digitizer must be Count (c) for %s\n", adc->label);
      }
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_POLES")) {
      errors("%s\n", "NUMBER_OF_POLES is not a valid keyword for digitizer");
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_ZEROES")) {
      errors("%s\n", "NUMBER_OF_ZEROES is not a valid keyword for digitizer");
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_COEFFICIENTS")) {
      /* Nothing to do */
		} else if (!strcasecmp(current->value_name, "RESPONSE_NAME")) {
      /* Nothing to do */
    } else if (!strcasecmp(current->value_name, "RESPONSE_TYPE")) {
      /* Nothing to do */
		} else {
			warnings("The keyword '%s' is unknown so we skip it\n",current->value_name);
		}
		current = current->next;
	}
	if (findTransfertFunctionType == FALSE) {
		errors("%s\n","can't find TRANSFER_FUNCTION_TYPE for %s\n", adc->label);
	}

	if (findSensitivity == FALSE) {
	  errors("can't find SENSITIVITY for %s\n", adc->label);
	}

	if (findOutputSamplingInterval == FALSE) {
	  errors("can't find OUTPUT_SAMPLING_INTERVAL for %s\n", adc->label);
	}
}
