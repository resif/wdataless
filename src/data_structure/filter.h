/**\file filter.h
 *
 */

#ifndef FILTER_H_
#define FILTER_H_

/**\enum filter_type
 *
 * \brief Filter types.
 */
typedef enum filter_type {
  UNDEF_FILT = 0,
  ANALOG,         /**< Analog filter, poles & zeroes in Hertz */
  FIR_SYM_1,      /**< Symetrical FIR, odd nb of weigths */
  IIR_PZ,         /**< Recursive filter expressed with poles & zeroes */
  COMB,           /**< FIR filter with all weigths equal */
  IIR_POLY,       /**< Recursive filter expressed with coefficients */
  FIR_ASYM,       /**< Asymetrical FIR */
  FIR_SYM_2,      /**< Symetrical FIR, even nb of weigths */
  LAPLACE,        /**< Analog filter, poles & zeroes in radians / sec */
  POLYNOMIAL      /**< Polynomial transformation (for sensor like temperature, pressure, wind, ... )*/
} filter_type_t;


/**\struct filter_s
 *
 */
struct filter_s {
	filter_type_t type;           /**< filter type ANALOG  FIR  IIR  COMB  IIR_POLY */
	int ncoef;                    /**< num of coefs (if type FIR | COMB | IIR_POLY) */
	int nzero;                    /**< num of zeros (if type ANALOG | IIR)          */
	int npole;                    /**< num of poles (if type ANALOG | IIR)          */
	int decim;                    /**< decimation factor (1 => no decimation)       */
	double delay;                 /**< Estimated delay (seconds) blk 57             */
	double correction;            /**< Correction applied (seconds) blk 57          */
	double sint;                  /**< sample interval                              */
	double A;                     /**< stantdard normalization factor               */
	double A0;                    /**< Seed normalization factor                    */
	double S;                     /**< standard sensitivity                         */
	double Sd;                    /**< Seed sensitivity                             */
	double fn;                    /**< normalization frequency                      */
	double gain;                  /**< filter gain                                  */
	double *coef;                 /**< array of coefs (if FIR | COMB | IIR_POLY)    */
	double *zero;                 /**< array of zeros (if ANALOG | IIR)             */
	double *pole;                 /**< array of poles (if ANALOG | IIR)             */
	double lower_bound_approx;    /**< if POLYNOMIAL                                */
  double upper_bound_approx;    /**< if POLYNOMIAL                                */
  char   valid_frequency_unit;  /**< if POLYNOMIAL  (A or B)                      */
};

/**
 * \brief filter_t type definition.
 */
typedef struct filter_s *filter_t;

/**\fn void verify_normalization_frequency(filter_t filter)
 *
 * \brief Verify that normalization frequency is pan.
 *
 * \param filter The filter.
 *
 * \return  0 if all is ok and a non zero value if error.
 */
int verify_normalization_frequency(filter_t filter);

/**\fn void calc_filter_A0(filter_t filter)
 *
 * \brief Compute A0 value for a given filter.
 *
 * \param filter The filter.
 */
void calc_filter_A0(filter_t);

/**\fn filter_t create_filter()
 *
 * Initialize an empty filter structure.
 *
 * @return The newly created empty filter structure.
 */
filter_t create_filter();

#endif /* FILTER_H_ */
