/**\file filter.c
 *
 *
 */

#include <math.h>
#include <float.h>
#include <complex.h>
#undef I
#include "errors.h"
#include "util.h"
#include "seed.h"
#include "filter.h"




/**\fn static void analog_trans(double *ze, int nz, double *po, int np, double h0, double f, double *out, filter_type_t type)
 *
 * \brief Compute response of analog filter.
 *
 * \param ze   The array zero coefficients
 * \param nz   The number of coefficients in ze array
 * \param po   The array pole coefficients
 * \param np   The number of coefficients in po array
 * \param h0
 * \param f    The normalization frequency
 * \param out  A double array (size = 2) for storing result
 * \param type The filter type
 *
 */
static void analog_trans(double *ze, int nz, double *po, int np, double h0,
    double f, double *out, filter_type_t type) {
  int i;
  double mod = 1.0, pha = 0.0;
  double R, I;

  if (type == LAPLACE) {
    f = TWOPI * f;
  }

  for (i = 0; i < 2 * nz; i += 2) {
    R = -ze[i];
    I = -ze[i + 1];
    if (R == 0.0 && I == 0.0) {
      mod *= f;
      pha += PI / 2.0;
    } else {
      mod *= sqrt((f + I) * (f + I) + R * R);
      pha += atan2(f + I, R);
    }
  }
  for (i = 0; i < 2 * np; i += 2) {
    R = -po[i];
    I = -po[i + 1];
    if (R == 0.0 && I == 0.0) {
      mod /= f;
      pha -= PI / 2.0;
    } else {
      mod /= sqrt((f + I) * (f + I) + R * R);
      pha -= atan2(f + I, R);
    }
  }
  out[0] = mod * cos(pha) * h0;
  out[1] = mod * sin(pha) * h0;
}


filter_t create_filter() {

  filter_t result = (filter_t) mem(sizeof(struct filter_s));

  result->decim = 1;
  result->ncoef = 0;
  result->nzero = 0;
  result->npole = 0;
  result->decim = 1;
  result->delay = 0;
  result->correction = 0;
  result->sint = 0;
  result->A = 0;
  result->A0 = 0;
  result->S = 1;
  result->Sd = 1;
  result->fn = 0;
  result->gain = 1;
  result->coef = NULL;
  result->zero = NULL;
  result->pole = NULL;
  result->lower_bound_approx = 0;
  result->upper_bound_approx = 0;
  result->valid_frequency_unit = '?';
  return result;
}


int verify_normalization_frequency(filter_t filter) {
  float min_freq = FLT_MAX, max_freq = 0;
  int i;

  if (filter->npole < 1)
    return 0;

  for (i=0; i<filter->npole; i++) {
    float t = 0;
    if (filter->type == IIR_PZ || filter->type == ANALOG) {
      t = sqrt(filter->pole[2*i] * filter->pole[2*i] +
               filter->pole[2*i+1] * filter->pole[2*i+1]);
    } else if (filter->type == LAPLACE) {
      t = sqrt(filter->pole[2*i] * filter->pole[2*i] +
               filter->pole[2*i+1] * filter->pole[2*i+1]) /  (2.0 * M_PI);
    } else {
      warnings("Normalization frequency verification can be made for filter type %d\n", filter->type);
      return 1;
    }
    if (t < min_freq) {
      min_freq = t;
    }
    if (t > max_freq) {
       max_freq = t;
    }
  }
  if (max_freq - min_freq < 0.1) {
      // No real pan
      // Look if we are not to far 
      float max_error = 0.05 * max_freq;
      if (fabs(filter->fn - max_freq) > max_error) {
        warnings("Normalization frequency (%f) to far from %f (difference is more that 5 %%)\n",
                 filter->fn, max_freq);
        return 1;
      }
      return 0; 
  }
  if (filter->fn < (min_freq - min_freq * 0.05) || filter->fn > (max_freq + max_freq * 0.05)) {
    warnings("Normalization frequency (%f) is not between (%f, %f)\n",
             filter->fn, min_freq, max_freq);
    return 1;
  }
  return 0;
}

void calc_iir_A0(filter_t filter) {
  complex double numerator = 0 + 0 * _Complex_I;
  complex double denominator = 0 + 0 * _Complex_I;
  complex double hp;
  complex double zn;
  int i;

  zn = cexp(0 + (2 * M_PI * _Complex_I * filter->fn * filter->sint));

  for (i = 0; i < 2 * filter->nzero; i += 2) {
    numerator += (zn - (filter->zero[i] + filter->zero[i+1] * _Complex_I));
  }
  for (i = 0; i < 2 * filter->npole; i += 2) {
    denominator += (zn - (filter->pole[i] + filter->pole[i+1] * _Complex_I));
  }
  hp = denominator / numerator;

  filter->A0 = sqrt(creal(hp) * creal(hp) + cimag(hp) * cimag(hp));
}

void calc_analog_A0(filter_t filter) {
  double *pz, *pp;
  double of[2];
  int nz, np;

  pz = &filter->zero[0];
  nz = filter->nzero;
  pp = &filter->pole[0];
  np = filter->npole;

  analog_trans(pz, nz, pp, np, 1.0, filter->fn, of, filter->type);
  filter->A0 = 1.0 / sqrt(of[0] * of[0] + of[1] * of[1]);
}

void calc_filter_A0(filter_t filter) {
  if (filter->type == IIR_PZ) {
    calc_iir_A0(filter);
  } else if (filter->type == ANALOG || filter->type == LAPLACE) {
    calc_analog_A0(filter);
  } else {
    errors("The type of filter is not managed (%d)", filter->type);
  }
}
