/**\file sensor.c
 *
 *
 */
#include <string.h>
#include "main.h"
#include "sensor.h"
#include "definition.h"
#include "util.h"    // For mem
#include "seed.h"    // For dbencode
#include "errors.h"  // For errors
#include "coef_file.h"
#include "filter.h"
#include "stage.h"
#include "units.h"


int comp_sensor(sensor_t sensor1, sensor_t sensor2) {
  return strcmp(sensor1->label, sensor2->label);
}

sensor_t create_sensor(char *label, char *type, char *serial_number,
		char *pz_file, double azimuth, double dip) {

	sensor_t result;

	result = (sensor_t) mem(sizeof(struct sensor_s));

	sprintf(result->label, "%s", label);

	result->type = (char *) mem(strlen(type) + 3);
	sprintf(result->type, "%s", type);

	char *meta_model = get_sensor_metamodel(result->type);

	if (dbencode(meta_model) < 0) {
		errors("unsupported sensor '%s'\n",result->type);
	}

	result->serial_number = (char *) mem(strlen(serial_number) + 1);
	sprintf(result->serial_number, "%s", serial_number);
	result->azimuth = azimuth;
	result->dip = dip;

	// Set default calibration unit
	result->calibrationUnits = get_unit_from_string("V");

	if (!file_exist_in_pz(pz_file)) {
		errors("'%s' is not a regular file or don't exist\n",pz_file);
	}
	result->sname = (char *) mem(strlen(get_pz_dir()) + strlen(pz_file) + 2);
	sprintf(result->sname, "%s/%s", get_pz_dir(), pz_file);

	parse_stage_file(result->sname, result);

	return result;
}

void process_sensor(sensor_t sensor, definition_t definition_list) {
	definition_t current = definition_list;

  char *include_file                  = NULL;
	int   findTransferFunction          = FALSE;
	int   findInputUnit                 = FALSE;
	int   findSensitivity               = FALSE;
	int   findTransferNormalisationFreq = FALSE;
  int   findLowerBound                = FALSE;
  int   findUpperBound                = FALSE;
  int   readInclude                   = FALSE;
  int   no_min_freq                   = FALSE;

	while (current != NULL) {
		if (!strcasecmp(current->value_name, "TRANSFER_FUNCTION_TYPE")) {
		  if (!strcasecmp((char *) current->value, "ANALOG")) {
		    sensor->filter->type = ANALOG;
		  } else if (!strcasecmp((char *) current->value, "LAPLACE")) {
		    sensor->filter->type = LAPLACE;
		  } else if (!strcasecmp((char *) current->value, "POLYNOMIAL")) {
        sensor->filter->type = POLYNOMIAL;
		  } else {
				errors("For sensor the TRANSFER_FUNCTION_TYPE must be ANALOG or LAPLACE and is %s\n",(char *)current->value);
			}
		  findTransferFunction = TRUE;
		} else if (!strcasecmp(current->value_name, "INPUT_UNIT")) {
			sensor->inpUnits = get_unit_from_string((char *) current->value);
			debug(DEBUG_STAGE_PARSER,"Sensor %s input unit : %s\n",sensor->label,(char *)current->value);
			findInputUnit = TRUE;
		} else if (!strcasecmp(current->value_name, "OUTPUT_UNIT")) {
			sensor->outUnits = get_unit_from_string((char *) current->value);
			if (strcmp(sensor->outUnits->name, "V")) {
				errors("Expect V for sensor output unit and got %s\n",(char *)current->value);
			}
		} else if (!strcasecmp(current->value_name, "SENSITIVITY")) {
			sensor->filter->Sd = *((double *) current->value);
			findSensitivity = TRUE;
		} else if (!strcasecmp(current->value_name,
				"TRANSFER_NORMALIZATION_FREQUENCY")) {
			sensor->filter->fn = *((double *) current->value);
			findTransferNormalisationFreq = TRUE;
			if (sensor->filter->fn  < 0) {
			  errors("TRANSFER_NORMALIZATION_FREQUENCY must be greater than 0 (%e)\n", sensor->filter->fn);
			}
		} else if (!strcasecmp(current->value_name,
		        "LOWER_BOUND_APPROXIMATION")) {
		  sensor->filter->lower_bound_approx = *((double *) current->value);
		  findLowerBound = TRUE;
    } else if (!strcasecmp(current->value_name,
            "UPPER_BOUND_APPROXIMATION")) {
      sensor->filter->upper_bound_approx = *((double *) current->value);
      findUpperBound = TRUE;
		} else if (!strcasecmp(current->value_name,
        "CALIBRATION_UNIT")) {
		  sensor->calibrationUnits = get_unit_from_string((char *) current->value);
		  if (strcmp(sensor->calibrationUnits->name, "V") &&
          strcmp(sensor->calibrationUnits->name, "A")) {
		    errors("CALIBRATION_UNIT must be V or A (<%s> given)\n", current->value);
		  }
    } else if (!strcasecmp(current->value_name,
        "VALID_FREQUENCY_UNIT")) {
        if (!strcasecmp((char *) current->value, "RAD/S")) {
      		   sensor->filter->valid_frequency_unit = 'A';
      	} else if (!strcasecmp((char *) current->value, "HZ")) {
    		   sensor->filter->valid_frequency_unit = 'B';
    		} else {
    			errors("For sensor the VALID_FREQUENCY_UNIT must be 'HZ' or 'RAD/S' and is %s\n",(char *)current->value);
    		}
		} else if (!strcasecmp(current->value_name, "INCLUDE")) {
		  if (! findTransferFunction) {
		    readInclude = TRUE;
		    include_file = (char *) current->value;
		  } else {
		    if (sensor->filter->type == POLYNOMIAL) {
		      if (read_sensor_poly_coef((char *) current->value, (filter_t) sensor->filter)
		          == FALSE) {
		        errors("%s\n","read sensor polynomial coefficients failed\n");
		      }
		    } else {
		      // Must load pz file
		      if (read_poles_and_zeros((char *) current->value, (filter_t) sensor->filter)
		          == FALSE) {
		        errors("%s\n","readPZ failed\n");
		      }
		    }
		  }
		} else if (!strcasecmp(current->value_name, "NUMBER_OF_ZEROES")) {
			/* Nothing to do */
		} else if (!strcasecmp(current->value_name, "NUMBER_OF_POLES")) {
			/* Nothing to do */
    } else if (!strcasecmp(current->value_name, "NUMBER_OF_COEFFICIENTS")) {
      errors("%s\n", "NUMBER_OF_COEFFICIENTS is not a valid keyword for sensor");
		} else if (!strcasecmp(current->value_name, "RESPONSE_NAME")) {
      /* Nothing to do */
    } else if (!strcasecmp(current->value_name, "NO_MIN_FREQ")) {
      no_min_freq = TRUE;
    } else if (!strcasecmp(current->value_name, "RESPONSE_TYPE")) {
      /* Nothing to do */
    } else if (!strcasecmp(current->value_name, "POLES_AND_ZEROES_UNIT")) {
      /* Nothing to do */
		} else {
			warnings("The keyword '%s' is unknown so we skip it\n",current->value_name);
		}
		current = current->next;
	}

  // Verify that Transfer function type is defined and read
	// include file if need and not yet done
  if(!findTransferFunction) {
    errors("Can't find TRANSFER_FUNCTION_TYPE in %s\n", sensor->sname);
  }

  if (sensor->filter->type == POLYNOMIAL) {
    /* VALID_FREQUENCY_UNIT must be defined */
    if (sensor->filter->valid_frequency_unit == '?') {
      errors("%s\n", "TRANSFER_FUNCTION_TYPE is POLYNOMIAL and VALID_FREQUENCY_UNIT is not set");
    }
  }
	if (readInclude) {
    if (sensor->filter->type == POLYNOMIAL) {
      if (read_sensor_poly_coef(include_file, (filter_t) sensor->filter)
          == FALSE) {
        errors("%s\n","read sensor polynomial coefficients failed");
      }
    } else {
      // Must load pz file
      if (read_poles_and_zeros(include_file, (filter_t) sensor->filter)
          == FALSE) {
        errors("%s\n","readPZ failed");
      }
    }
	}


  // Verify that mandatory definitions is presents
	if(!findInputUnit) {
	    errors("Can't find INPUT_UNIT in %s\n", sensor->sname);
	}

	if(!findSensitivity && sensor->filter->type != POLYNOMIAL) {
	    errors("Can't find SENSITIVITY in %s\n", sensor->sname);
	}

	if(!findTransferNormalisationFreq && sensor->filter->type != POLYNOMIAL) {
	    errors("Can't find TRANSFER_NORMALIZATION_FREQUENCY in %s\n", sensor->sname);
	}

	if(sensor->filter->type == POLYNOMIAL && ! findLowerBound) {
    errors("Can't find LOWER_BOUND_APPROXIMATION in %s\n", sensor->sname);
	}

  if(sensor->filter->type == POLYNOMIAL && ! findUpperBound) {
    errors("Can't find UPPER_BOUND_APPROXIMATION in %s\n", sensor->sname);
  }

  if (sensor->filter->type != POLYNOMIAL ) {
    // Verify poles values
    if (check_roots(sensor->filter->pole, sensor->filter->npole,
        sensor->filter->type, "pole") == FALSE) {
      errors("%s\n","check_roots: poles: failed");
    }

    // Verify zeroes values
    if (check_roots(sensor->filter->zero, sensor->filter->nzero,
        sensor->filter->type, "zero") == FALSE) {
      errors("%s\n","check_roots: zeroes: failed");
    }

    calc_filter_A0(sensor->filter);
    if (! no_min_freq && strcmp(sensor->inpUnits->seed_code, "M/S**2")) {
      if (verify_normalization_frequency(sensor->filter) != 0) {
        errors("Error when checking normalization frequency in %s\n", sensor->sname);
      }
    } 
  }
}
