/**\file  owner.c
 *
 *
 */
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "errors.h"
#include "owner.h"
#include "blk.h"
#include "util.h"
#include "channel.h"


// The list of owner
static owner_t owner_list = NULL;

/**\fn int get_owner_abbreviation_index(char *owner_name)
 *
 * Find the index of a owner name in the abbreviation list.
 *
 * \param owner_name The owner name
 *
 * \return The index of the owner name or -1 if not find.
 *
 */
int get_owner_abbreviation_index(char *owner_name) {

	owner_t current;
	int     index = 1;
	for(current = owner_list;current != NULL; current = current->next) {
		if(!strcmp(owner_name,current->owner_name)) {
			return index + get_super_type_list_size();
		}
		index++;
	}
	return -1;
}

/**\fn void add_owner_name(char *new_owner_name)
 *
 * Add a new owner name to the abbreviations list.
 * We must note that the presence of the name is tested
 * before adding it.
 *
 * \param new_owner_name The owner name to add
 */
void add_owner_name(char *new_owner_name) {
	if(get_owner_abbreviation_index(new_owner_name) == -1) {
		owner_t new_node;
		new_node = (owner_t) mem(sizeof(struct struct_owner));
		new_node->owner_name = (char *) mem(strlen(new_owner_name) + 1);
		strcpy(new_node->owner_name,new_owner_name);
		new_node->next = owner_list;
		owner_list = new_node;
	}
}

/**\fn char *owner_iterator(int init)
 *
 * Iterator function for owner list.
 *
 * \param init TRUE for the first call (initialization) and FALSE for next call.
 *
 * \return Always NULL when init is TRUE
 *         When init is FALSE, the next list element or NULL when end list is reach.
 *
 */
char *owner_iterator(int init) {
	static owner_t  list   = NULL;
	char           *result = NULL;

	if(init) {
		list = owner_list;
	} else if (list != NULL){
		result =  list->owner_name;
		list = list->next;
	}
	return result;
}
