/**\file digitizer.h
 *
 *
 */

#ifndef DIGITIZER_H_
#define DIGITIZER_H_

/**
 * Define analogic filter type.
 */
typedef struct digitizer_s *digitizer_t;

#include "digital_filter.h"

/**\struct digitizer_s
 *
 * \brief digitizer data representation.
 */
struct digitizer_s {
  long object_type; /**< The object type      */
  char label[10]; /**< digitizer label               */
  double fact; /**< digitizer conversion constant */
  double sint; /**< sample interval, seconds      */
  int dataformat; /**< data format: 1|2|3            */
};

/**\fn int comp_digitizer(digitizer_t digitizer1, digitizer_t digitizer2);
 *
 * \brief say if two digitizer_t have the same label.
 *
 * digitizer1 -- The first digitizer
 * digitizer2 -- The second digitizer
 *
 * return     -- 0 if the digitizers have the same label.
 */
int comp_digitizer(digitizer_t digitizer1, digitizer_t digitizer2);

/**\fn int is_adc(digitizer_t adc)
 *
 * \brief Say if a given pointer is an digitizer_t
 *
 * \param adc The tested pointer
 *
 * \return TRUE if adc is an digitizer_t and FALSE otherwise
 */
int is_adc(digitizer_t adc);

/**\def define check_is_adc(adc)
 *
 */
#define check_is_adc(adc)  if(!is_adc(((digitizer_t)(adc)))) { \
  errors("Pointer %p is not an ADC\n",adc);\
  }

/**\fn digitizer_t create_adc(char *label,char *stage_file)
 *
 * Create a new ADC.
 *
 * \param label      -- The label of the new digitizer_t
 * \param stage_file -- The file used to create the digitizer_t
 *
 * \return The newly created  digitizer_t
 *
 */
digitizer_t create_adc(char *label, char *stage_file);

/**\fn void process_adc(digitizer_t adc, definition_t definition_list)
 *
 * \brief Fill an digitizer_t according to a definition_s list and a cascade filter.
 *
 * \param adc             The digitizer_t to fill
 * \param definition_list The definition_s list
 *
 */
void process_adc(digitizer_t adc, definition_t definition_list);

#endif /* ADC_H_ */
