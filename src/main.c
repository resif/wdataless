/**\file main.c
 *
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pwd.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include "main.h"
#include "util.h"
#include "station.h"
#include "blk.h"
#include "errors.h"
#include "dbird.h"
#include "dataless.h"

static void parse_arg(int, char **);
static void dbird2seed();
static void help();


static char **resplist          = NULL;
static int    keep_channel_name = 0;
char         *vol_fname         = "seedvol";
static char   blk[20000];
static int    nrespfiles;
char          seed_label[100];


int main(int argc, char *argv[]) {
  char ttime[40];
  time_t tt;
  struct passwd *pw_ent;

  //set_debug_mode(DEBUG_ALL);
  parse_arg(argc, argv);

  messages("Receive %d files to process\n", nrespfiles);
  /* Get current time */
  time(&tt);

  /* Get user name */
  pw_ent = getpwuid(getuid());
  strftime(ttime, 40, "%Y.%m.%d-%H:%M:%S", gmtime(&tt));
  sprintf(blk, "File '%s' created on '%s' by '%s'", vol_fname, ttime,
      pw_ent->pw_name);
  sprintf(seed_label, "%.78s", blk);

  time(&tt);
  /* Get user name */
  pw_ent = getpwuid(getuid());
  strftime(ttime, 40, "%Y.%m.%d-%H:%M:%S", gmtime(&tt));
  sprintf(blk, "File '%s' created on '%s' by '%s'", vol_fname, ttime,
      pw_ent->pw_name);
  sprintf(seed_label, "%.78s", blk);

  dbird2seed();

  return 0;
}

/**\fn static void load_dbird_files(char **dbirdarray, int nfiles)
 *
 * Load all given dbird files.
 *
 * \param dbirdarray The array of dbird filename
 * \param nfiles     The number of filename in dbirdlist
 *
 */
static void load_dbird_files(char **dbirdarray, int nfiles) {
  station_t result;
  station_t tmp;
  int i;

  for (i = 0; i < nfiles; i++) {
    parse_dbird_file(dbirdarray[i], &result);
    tmp = result;
    while(tmp->next != NULL)
      tmp = tmp->next;
    tmp->next = stalist_head;
    stalist_head = result;
  }
}

/**
 *
 */
static void dbird2seed() {

  /* Open SEED volume name */
  FILE *Fp_volume = NULL;

  if (!(Fp_volume = fopen(vol_fname, "w"))) {
    errors("ERROR: wdataless: can't open %s\n", vol_fname);
  }

  /* Load unit description from unit_list*/
  read_unit_list();

  /* Load sensor description from sensor_list*/
  read_sensor_list();

  /* load station-channels response */
  load_dbird_files(resplist, nrespfiles);

  /* Check seed channel name */
  check_seed_chan_name(keep_channel_name);

  /* Look for dataless SEED volume */
  write_dataless(vol_fname, Fp_volume);
  return;
}

/**\fn static int parse_arg(int argc, char **argv)
 *
 *  Parse the command line.
 *  If argument is not recognized exit() is called.
 *
 *  \param argc Number of arguments
 *  \param argv The list of arguments
 *
 */
static void parse_arg(int argc, char **argv) {
  int i;

  if (argc < 1) {
    help();
  }
  for (i = 1; i < argc; i++) {
    /* Response input file */
    if (!strcmp(argv[i], "-r")) {
      resplist = &argv[i + 1];
      nrespfiles = 0;
      while (++i < argc && argv[i][0] != '-') {
        ++nrespfiles;
      }
      --i;
    } else if (!strcmp(argv[i], "-o") && (i + 1) < argc) {
      /* Changing default output Seed volume name ("seed") */
      ++i;
      if (argv[i][0] != '-') {
        vol_fname = (char *) mem(strlen(argv[i]) + 1);
        sprintf(vol_fname, "%s", argv[i]);
      } else {
        help();
      }
    } else if (!strcmp(argv[i], "--keep-channel-name")) {
      keep_channel_name = 1;
    } else if (!strcmp(argv[i], "--verbose")) {
      set_verbose_level(2);
    } else if (!strcmp(argv[i], "--quiet")) {
      set_verbose_level(0);
    } else if (!strcmp(argv[i], "-h")) {
      /* Print out help */
      help();
    } else {
      fprintf(stderr, "\n\n option '%s' not supported\n\n", argv[i]);
      help();
    }
  }
}
/**\fn static void help()
 *
 * Usage function.
 *
 * Print help message and exit.
 */
static void help() {

  printf("\n");
  printf("  This program creates a SEED DATALESS\n");
  printf("  We use \"PZ\" database directory\n");
  printf(
      "(PZ environment variable) plus a set of stations information files.\n");
  printf("\n");
  printf("\n");
  printf("  Usage: %s -r filenames [options]\n", PROGRAM_NAME);
  printf("    Multiple dbird files and wild cards supported.\n");
  printf("\n");
  printf("\n");
  printf("  Options:\n");
  printf("\n");
  printf("    -o name             -- SEED output file name or device\n");
  printf("                           Default: 'seedvol'\n");
  printf("\n");
  printf("    --keep-channel-name -- Keep channel name even if error is detected");
  printf("\n");
  printf("    --verbose           -- Enable displaying of information messages");
  printf("\n");
  printf("    --quiet             -- Disable displaying of warning messages");
  printf("\n");
  printf("  Examples:\n");
  printf("    For \"PZ\" database:\n");
  printf("        setenv PZ /home/fels/PZ\n");
  printf("        %s -r dbird.20020515\n", PROGRAM_NAME);
  printf("\n");
  printf("  Machine Word order:\n");
  printf("      If %s is run on a Linux machine, steim compressed data are\n",
      PROGRAM_NAME);
  printf("      swapped as if they had been created on a Sun machine\n");

  printf("\n");

  exit(0);
}
