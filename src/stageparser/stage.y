// Change prefix
%name-prefix "stage"

%{
  int stageerror(char *errmsg);
  typedef struct pz_data    *pz_data_t;
#include "list.h"
#include "errors.h"
#include "util.h"
#include "definition.h"
#include "sensor.h"
#include "analog_filter.h"
#include "digitizer.h"
#include "digital_filter.h"
#include "stageparser.h"
#include "stagelex.h"

///////////////////////////////////////////////////////////////////////
#ifndef YY_BUF_SIZE
#ifdef __ia64__
/* On IA-64, the buffer size is 16k, not 8k.
 * Moreover, YY_BUF_SIZE is 2*YY_READ_BUF_SIZE in the general case.
 * Ditto for the __ia64__ case accordingly.
 */
#define YY_BUF_SIZE 32768
#else
#define YY_BUF_SIZE 16384
#endif /* __ia64__ */
#endif
//////////////////////////////////////////////////////////////////////



    // The object filled by parsing
    static void     *current_object;
    static filter_t  current_filter = NULL;
    static char     *current_filename;

    struct pz_data {
    	double    val1;
    	double    val2;
    	pz_data_t next;
    };




    definition_t new_definition(char *,void *);
%}




// Define union used for token's type
%union {
  int                    int_value;
  double                 double_value;
  char                  *string;
  definition_t           definition;
  pz_data_t              pz_data;
  analog_filter_stage_t  analog_filter_stage;
  digital_filter_t       digital_filter;
  filter_cascade_t       cascade;
  list_t                 list;
}

// Token returned by lexer
%token BEGIN_
%token END
%token SENSOR
%token ANALOG_FILTER
%token DIGITIZER
%token DIGITAL_FILTER
%token STAGE_FILTER


// Define tokens types
%token <int_value>      INTEGER
%token <double_value>   DOUBLE
%token <string>         STRING
%token <string>         WORD
%token <string>         KEYWORD
%token <string>		    NUMBER_OF_ZEROES
%token <string>		    NUMBER_OF_POLES
%token <string>         NUMBER_OF_COEFFICIENTS
%token <string>         INCLUDE

 // Define rules types
%type  <cascade>              stage_filter_list
%type  <analog_filter_stage>  analog_filter_stage
%type  <digital_filter>       stage_filter
%type  <definition>           definition
%type  <definition>           definitions_list
%type  <pz_data>              pz_data
%type  <pz_data>              coefs_list
%type  <digital_filter>       digital_filter

%%

///////
//
// Grammar rules
//
///////


file: elements_list
;

elements_list : BEGIN_ element
;

element : sensor
| analog_filters
| digitizer
| digital_filters_cascade
| END
{
	stageerror("Closing end without begin");
}
;

sensor: SENSOR definitions_list END SENSOR
{
	// Get the last created sensor
	sensor_t sensor = (sensor_t)current_object;

	// Fill the new created sensor object
	if (current_filter != NULL) {
		sensor->filter = current_filter;
		current_filter = NULL;
	} else {
		sensor->filter = (filter_t) mem(sizeof(struct filter_s));
		sensor->filter->nzero = 0;
		sensor->filter->npole = 0;
	}
	sensor->filter->type = UNDEF_FILT;
	sensor->filter->ncoef = 0;
	sensor->filter->decim = 1;
	sensor->filter->sint = 0.0;
	sensor->filter->A = 1.0;
	sensor->filter->gain = 1.0;
	sensor->filter->A0 = 1.0;
	sensor->filter->Sd = 1.0;
	sensor->inpUnits = 0;
	sensor->outUnits = 0;

	process_sensor(sensor,$2);
}

;

analog_filters: analog_filter_stage
{
  ((analog_filter_t)current_object)->nstage += 1;
  ((analog_filter_t)current_object)->stages = (analog_filter_stage_t *) mem_realloc(((analog_filter_t)current_object)->stages,
                                                                                    ((analog_filter_t)current_object)->nstage * sizeof(struct analog_filter_stage_s));
  ((analog_filter_t)current_object)->stages[((analog_filter_t)current_object)->nstage-1] = $1;
}
| analog_filters  BEGIN_ analog_filter_stage
{
  ((analog_filter_t)current_object)->nstage += 1;
  ((analog_filter_t)current_object)->stages = (analog_filter_stage_t *) mem_realloc(((analog_filter_t)current_object)->stages,
                                                                                    ((analog_filter_t)current_object)->nstage * sizeof(struct analog_filter_stage_s));
  ((analog_filter_t)current_object)->stages[((analog_filter_t)current_object)->nstage-1] = $3;
}
;

analog_filter_stage:  ANALOG_FILTER definitions_list END ANALOG_FILTER
{
  analog_filter_stage_t result = create_analog_filter_stage();
  if(current_filter != NULL) {
    result->filter = current_filter;
    current_filter = NULL;
  } else {
    result->filter = create_filter();
  }
  process_analog_filter_stage(current_object, result, $2);
  $$ = result;
}
;

digitizer : DIGITIZER definitions_list END DIGITIZER
{
	process_adc(current_object,$2);
}
| DIGITIZER definitions_list END STAGE_FILTER
{
	stageerror("begin digitizer is closed with end stage");
}
;

digital_filters_cascade: digital_filter stage_filter_list
{
	int i;
	if(!is_cascade($2)) {
		errors("The pointer %p is not filter_cascade_t\n",$2);
	}


	((filter_cascade_t)current_object)->nstage = 1 + $2->nstage;
	((filter_cascade_t)current_object)->digital_filter = (digital_filter_t *) mem((1 + $2->nstage) * sizeof(digital_filter_t));
	((filter_cascade_t)current_object)->digital_filter[0] = $1;
	for(i=0;i<$2->nstage;i++) {
		((filter_cascade_t)current_object)->digital_filter[i+1] = $2->digital_filter[i];
	}
}
 | digital_filter
{
	((filter_cascade_t)current_object)->nstage = 1;
	((filter_cascade_t)current_object)->digital_filter = (digital_filter_t *) mem(sizeof(digital_filter_t));
	((filter_cascade_t)current_object)->digital_filter[0] = $1;
        if (((filter_cascade_t)current_object)->digital_filter[0]->is_fake) {
          ((filter_cascade_t)current_object)->is_fake = 1;
        }
}

;

digital_filter: DIGITAL_FILTER definitions_list END DIGITAL_FILTER
{
	digital_filter_t result = mem(sizeof(struct digital_filter_s));
	if(current_filter != NULL) {
		result->filter = current_filter;
		current_filter = NULL;
	} else {
		result->filter = (filter_t) mem(sizeof(struct filter_s));
	}
	process_digital_filter(result,$2);
	$$ = result;
}
;


stage_filter_list: stage_filter_list stage_filter
{
  $1->digital_filter = mem_realloc($1->digital_filter,sizeof(digital_filter_t) * ++($1->nstage));
  $1->digital_filter[$1->nstage-1] = $2;

  $$ = $1;
}
| stage_filter
{
	filter_cascade_t result = create_digital_filter_cascade("none",NULL);
	result->filename = mem(strlen(current_filename) + 1);
	sprintf(result->filename, "%s", current_filename);
	result->nstage = 1;
	result->digital_filter = mem(sizeof(digital_filter_t));
	result->digital_filter[0] = $1;

	$$ = result;
}
;

stage_filter: BEGIN_ STAGE_FILTER definitions_list END STAGE_FILTER
{
	digital_filter_t result = (digital_filter_t) mem(sizeof(struct digital_filter_s));
	if(current_filter != NULL) {
		result->filter = current_filter;
		current_filter = NULL;
	} else {
		result->filter = (filter_t) mem(sizeof(struct filter_s));
	}
  	process_digital_filter_stage(result,$3);
  	$$ = result;
}
;

definitions_list : definition
{
	$1->next = NULL;
	$$ = $1;
}
| definitions_list definition
{
	$$ = $2;
	$2->next = $1;
}
;


definition: KEYWORD ':' STRING
{
  $$ = new_definition($1,$3);
}
| KEYWORD ':' WORD
{
  $$ = new_definition($1,$3);
}
| KEYWORD ':' INTEGER
{
	double *tmp = mem(sizeof(double));
	*tmp = (double)$3;
	$$ = new_definition($1,tmp);
}
| KEYWORD ':' DOUBLE
{
	double *tmp = mem(sizeof(double));
	*tmp = $3;
	$$ = new_definition($1,tmp);
}
| INCLUDE STRING
{
    $$ = new_definition($1,$2);
}
| NUMBER_OF_POLES ':' INTEGER pz_data
{
	int i;
	if (current_filter == NULL) {
		current_filter = mem(sizeof(struct filter_s));
	}
	current_filter->npole = $3;
	current_filter->pole = (double *) mem(current_filter->npole * sizeof(double) * 2);

	pz_data_t current = $4;
	// The coeff are stored in reverse order
	for(i=current_filter->npole-1;i>=0;i--) {
		if(current == NULL) {
			errors("Expected %d pole and find only %d\n",
              current_filter->npole,
              current_filter->npole-(i-1));
		}
		current_filter->pole[2 * i] = current->val1;
		current_filter->pole[2 * i + 1] = current->val2;
		current = current->next;
	}
  if (current != NULL) {
    errors("Expected %d poles and find more, The first excess pole is (%f, %f)",
           current_filter->npole, current->val1, current->val2);
  }
	double *tmp = mem(sizeof(double));
	*tmp = (double)$3;
    $$ = new_definition($1,tmp);
}
| NUMBER_OF_POLES ':' INTEGER
{
	if (current_filter == NULL) {
		current_filter = mem(sizeof(struct filter_s));
	}
	current_filter->npole = 0;
	double *tmp = mem(sizeof(double));
	*tmp = (double)$3;
    $$ = new_definition($1,tmp);
}
| NUMBER_OF_ZEROES ':' INTEGER pz_data
{
	int  i;

	if (current_filter == NULL) {
		current_filter = mem(sizeof(struct filter_s));
	}

	current_filter->nzero = $3;
	current_filter->zero = (double *) mem(current_filter->nzero * sizeof(double) * 2);
	pz_data_t current = $4;
	// The coeff are stored in reverse order
	for(i=current_filter->nzero-1;i>=0;i--) {
		if(current == NULL) {
      errors("Expected %d zero and find only %d\n",
             current_filter->nzero,
             current_filter->nzero-(i-1));
		}
		current_filter->zero[2 * i] = current->val1;
		current_filter->zero[2 * i + 1] = current->val2;
		current = current->next;
	}
  if (current != NULL) {
    errors("Expected %d zeros and find more, The first excess zero is (%f, %f)",
           current_filter->npole, current->val1, current->val2);
  }
	double *tmp = mem(sizeof(double));
	*tmp = (double)$3;
    $$ = new_definition($1,tmp);
}
| NUMBER_OF_ZEROES ':' INTEGER
{
	if (current_filter == NULL) {
		current_filter = mem(sizeof(struct filter_s));
	}
	current_filter->nzero = 0;
	double *tmp = mem(sizeof(double));
	*tmp = (double)$3;
    $$ = new_definition($1,tmp);
}
| NUMBER_OF_COEFFICIENTS ':' INTEGER
{
	if (current_filter == NULL) {
		current_filter = mem(sizeof(struct filter_s));
	}
	current_filter->ncoef = 0;
	double *tmp = mem(sizeof(double));
	*tmp = (double)$3;
    $$ = new_definition($1,tmp);
}
| NUMBER_OF_COEFFICIENTS ':' INTEGER coefs_list
{
	int  i;

	if (current_filter == NULL) {
		current_filter = mem(sizeof(struct filter_s));
	}

	current_filter->ncoef = $3;
	current_filter->coef = (double *) mem(current_filter->ncoef * sizeof(double));
	pz_data_t current = $4;
	// The coeff are stored in reverse order
	for(i=current_filter->ncoef-1;i>=0;i--) {
		if(current == NULL) {
			errors("%s\n","Internal Parser Error");
		}
		current_filter->coef[i] = current->val1;
		current = current->next;
	}
  if (current != NULL) {
    errors("Expected %d coefficients and find more, The first excess coefficient is %f",
           current_filter->npole, current->val1);
  }
	double *tmp = mem(sizeof(double));
	*tmp = (double)$3;
    $$ = new_definition($1,tmp);
}
;




// List of poles and zeros coefficients
// We must note that value are stored in reverse order
pz_data :  DOUBLE DOUBLE
{
	$$ = mem(sizeof(struct pz_data));
	$$->val1 = $1;
	$$->val2 = $2;
	$$->next = NULL;
}
| pz_data DOUBLE DOUBLE
{
	pz_data_t new = mem(sizeof(struct pz_data));
	new->val1 = $2;
	new->val2 = $3;
	new->next = $1;
	$$ = new;
}
;


coefs_list : DOUBLE
{
	$$ = mem(sizeof(struct pz_data));
	$$->val1 = $1;
	$$->next = NULL;
}
| coefs_list DOUBLE
{
	pz_data_t new = mem(sizeof(struct pz_data));
	new->val1 = $2;
	new->next = $1;
	$$ = new;
}
;

%%


///////
//
// C code
//
///////


void parse_stage_file(char *filename,void *object) {

	YY_BUFFER_STATE  buffer;
	FILE            *stage_file;

	current_filename = filename;
	debug(DEBUG_STAGE_PARSER,"Reading file %s\n",filename);
	current_object = object;
	if((stage_file = fopen(filename,"r")) == NULL) {
		errors("%s when try to open %s\n", strerror(errno), filename);
	}
	buffer = stage_create_buffer(stage_file, YY_BUF_SIZE);
	stage_switch_to_buffer(buffer);
	stagelineno = 1;
	stageparse();
        fclose(stage_file);
}


/**\fn int stageerror(char *errmsg)
 *
 * \brief Print an error message and exit.
 */
int stageerror(char *errmsg) {
	fprintf(stderr,"%s:%d : %s when reading %s\n",current_filename,stagelineno,errmsg,stagetext);
	exit(1);
}
