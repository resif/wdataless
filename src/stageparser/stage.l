%{
typedef struct pz_data    *pz_data_t;
#include <stdlib.h>
#include "definition.h"
#include <analog_filter.h>
#include "list.h"
#include "errors.h"
#include "digital_filter.h"
#include "stageparser.h"
#define YY_NO_INPUT
%}
%option noyywrap
%option yylineno
%option case-insensitive
%option prefix="stage"
%option outfile="stagelex.c"
%option nounput
%%



"#".* /* ignore comments */
"BEGIN"           {debug(DEBUG_STAGE_PARSER,"%s\n","BEGIN");stagelval.string = strdup(yytext) ;return BEGIN_;}
"END"             {debug(DEBUG_STAGE_PARSER,"%s\n","END");stagelval.string = strdup(yytext) ;return END;}
"SENSOR"          {debug(DEBUG_STAGE_PARSER,"%s\n","SENSOR");stagelval.string = strdup(yytext) ;return SENSOR;}
"ANA_FILTER"      {debug(DEBUG_STAGE_PARSER,"%s\n","ANALOG_FILTER");stagelval.string = strdup(yytext) ;return ANALOG_FILTER;}
"DIGITIZER"       {debug(DEBUG_STAGE_PARSER,"%s\n","DIGITIZER");stagelval.string = strdup(yytext) ;return DIGITIZER;}
"DIGITAL_FILTER"  {debug(DEBUG_STAGE_PARSER,"%s\n","DIGITAL_FILTER");stagelval.string = strdup(yytext) ;return DIGITAL_FILTER;}
"STAGE"           {debug(DEBUG_STAGE_PARSER,"%s\n","STAGE_FILTER");stagelval.string = strdup(yytext) ;return STAGE_FILTER;}

"RESPONSE_TYPE"                    {debug(DEBUG_STAGE_PARSER,"%s\n","RESPONSE_TYPE");stagelval.string = strdup(yytext) ;return KEYWORD;}
"CORRECTION"                       {debug(DEBUG_STAGE_PARSER,"%s\n","CORRECTION");stagelval.string = strdup(yytext) ;return KEYWORD;}
"DECIMATION_FACTOR"                {debug(DEBUG_STAGE_PARSER,"%s\n","DECIMATION_FACTOR");stagelval.string = strdup(yytext) ;return KEYWORD;}
"DELAY"                            {debug(DEBUG_STAGE_PARSER,"%s\n","DELAY");stagelval.string = strdup(yytext) ;return KEYWORD;}
"INCLUDE"                          {debug(DEBUG_STAGE_PARSER,"%s\n","INCLUDE");stagelval.string = strdup(yytext) ;return INCLUDE;}
"INPUT_FULL_SCALE"                 {debug(DEBUG_STAGE_PARSER,"%s\n","INPUT_FULL_SCALE");stagelval.string = strdup(yytext) ;return KEYWORD;}
"INPUT_SAMPLING_INTERVAL"          {debug(DEBUG_STAGE_PARSER,"%s\n","INPUT_SAMPLING_INTERVAL");stagelval.string = strdup(yytext) ;return KEYWORD;}
"INPUT_UNIT"                       {debug(DEBUG_STAGE_PARSER,"%s\n","INPUT_UNIT");stagelval.string = strdup(yytext) ;return KEYWORD;}
"NUMBER_OF_POLES"                  {debug(DEBUG_STAGE_PARSER,"%s\n","NUMBER_OF_POLES");stagelval.string = strdup(yytext) ;return NUMBER_OF_POLES; }
"NUMBER_OF_ZEROES"                 {debug(DEBUG_STAGE_PARSER,"%s\n","NUMBER_OF_ZEROES");stagelval.string = strdup(yytext) ;return NUMBER_OF_ZEROES;}
"NUMBER_OF_COEFFICIENTS"           {debug(DEBUG_STAGE_PARSER,"%s\n","NUMBER_OF_COEFFICIENTS");stagelval.string = strdup(yytext) ;return NUMBER_OF_COEFFICIENTS;}
"OUTPUT_FULL_SCALE"                {debug(DEBUG_STAGE_PARSER,"%s\n","OUTPUT_FULL_SCALE");stagelval.string = strdup(yytext) ;return KEYWORD;}
"OUTPUT_SAMPLING_INTERVAL"         {debug(DEBUG_STAGE_PARSER,"%s\n","OUTPUT_SAMPLING_INTERVAL");stagelval.string = strdup(yytext) ;return KEYWORD;}
"OUTPUT_UNIT"                      {debug(DEBUG_STAGE_PARSER,"%s\n","OUTPUT_UNIT");stagelval.string = strdup(yytext) ;return KEYWORD;}
"POLES_AND_ZEROES_UNIT"            {debug(DEBUG_STAGE_PARSER,"%s\n","POLES_AND_ZEROES_UNIT");stagelval.string = strdup(yytext) ;return KEYWORD;}
"RESPONSE_NAME"                    {debug(DEBUG_STAGE_PARSER,"%s\n","RESPONSE_NAME");stagelval.string = strdup(yytext) ;return KEYWORD;}
"SENSITIVITY"                      {debug(DEBUG_STAGE_PARSER,"%s\n","SENSITIVITY");stagelval.string = strdup(yytext) ;return KEYWORD;}
"TRANSFER_FUNCTION_TYPE"           {debug(DEBUG_STAGE_PARSER,"%s\n","TRANSFER_FUNCTION_TYPE");stagelval.string = strdup(yytext) ;return KEYWORD;}
"TRANSFER_NORMALIZATION_CONSTANT"  {debug(DEBUG_STAGE_PARSER,"%s\n","TRANSFER_NORMALIZATION_CONSTANT");stagelval.string = strdup(yytext) ;return KEYWORD;}
"TRANSFER_NORMALIZATION_FREQUENCY" {debug(DEBUG_STAGE_PARSER,"%s\n","TRANSFER_NORMALIZATION_FREQUENCY");stagelval.string = strdup(yytext) ;return KEYWORD;}
"POLES_AND_ZEROS_UNIT"             {debug(DEBUG_STAGE_PARSER,"%s\n","POLES_AND_ZEROS_UNIT");stagelval.string = strdup(yytext) ;return KEYWORD;}
"BEGIN_TIME"                       {debug(DEBUG_STAGE_PARSER,"%s\n","BEGIN_TIME");stagelval.string = strdup(yytext) ;return KEYWORD;}
"CALIBRATION_UNIT"                 {debug(DEBUG_STAGE_PARSER,"%s\n","CALIBRATION_UNIT");stagelval.string = strdup(yytext) ;return KEYWORD;}
"LOWER_VALID_FREQUENCY_BAND"       {debug(DEBUG_STAGE_PARSER,"%s\n","LOWER_VALID_FREQUENCY_BAND");stagelval.string = strdup(yytext) ;return KEYWORD;}
"UPPER_VALID_FREQUENCY_BAND"       {debug(DEBUG_STAGE_PARSER,"%s\n","UPPER_VALID_FREQUENCY_BAND");stagelval.string = strdup(yytext) ;return KEYWORD;}
"VALID_FREQUENCY_UNIT"             {debug(DEBUG_STAGE_PARSER,"%s\n","VALID_FREQUENCY_UNIT");stagelval.string = strdup(yytext) ;return KEYWORD;}
"LOWER_BOUND_APPROXIMATION"        {debug(DEBUG_STAGE_PARSER,"%s\n","LOWER_BOUND_APPROXIMATION");stagelval.string = strdup(yytext) ;return KEYWORD;}
"UPPER_BOUND_APPROXIMATION"        {debug(DEBUG_STAGE_PARSER,"%s\n","UPPER_BOUND_APPROXIMATION");stagelval.string = strdup(yytext) ;return KEYWORD;}
"NO_MIN_FREQ"                      {debug(DEBUG_STAGE_PARSER,"%s\n","NO_MIN_FREQ");stagelval.string = strdup(yytext) ;return KEYWORD;}


":" {return *yytext; }
[0-9]+                                              { debug(DEBUG_STAGE_PARSER,"INTEGER:%i\n",atoi(yytext)); stagelval.int_value = atoi(yytext) ; return INTEGER ;}
[-+]?([0-9]*\.?[0-9]+|[0-9]+\.)([eE]([+-])?[0-9]+)? { debug(DEBUG_STAGE_PARSER,"DOUBLE:%e\n",atof(yytext)) ;stagelval.double_value = atof(yytext) ; return DOUBLE;}
[A-Za-z][A-Za-z0-9]+                                { debug(DEBUG_STAGE_PARSER,"WORD:%s\n",yytext) ;stagelval.string = strdup(yytext) ; return WORD;}
"\""[^"]*"\""                                       { debug(DEBUG_STAGE_PARSER,"STRING:%s\n",yytext) ; stagelval.string = strdup(yytext+1) ; *(stagelval.string+strlen(stagelval.string)-1)='\0'; return STRING ;}
"\n" /* Can add line count */
. /* skip not match c*/
%%
